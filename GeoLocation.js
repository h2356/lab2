function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    } else {
        Location_P.innerHTML = "Geolocation is not supported by this browser.";
    }
}

function showPosition(position) {
    Location_P.innerHTML = "Latitude: " + position.coords.latitude +
        "<br>Longitude: " + position.coords.longitude;
}
